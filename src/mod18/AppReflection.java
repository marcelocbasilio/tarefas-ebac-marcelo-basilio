package mod18;

import mod18.annotation.Table;
import mod18.entities.Customer;

import java.lang.annotation.Annotation;

public class AppReflection {

    public static void main(String[] args) {

        Class<Customer> entityCustomer = Customer.class;
        Annotation[] annotations = entityCustomer.getAnnotations();

        for (Annotation annotation : annotations) {

            if (annotation instanceof Table tableAnnotation) {
                String customer = tableAnnotation.name();
                System.out.println("> Table name: " + customer);
            }
        }
    }
}
