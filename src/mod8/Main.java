package mod8;

public class Main {

    public static void main(String[] args) {

        Calcular calcular = new Calcular();
        calcular.setNota1(8.5d);
        calcular.setNota2(7.5d);
        calcular.setNota3(3.5d);
        calcular.setNota4(4.5d);

        Calcular calcular2 = new Calcular(9.1d, 8.3d, 7.5d, 3.5d);

        double mediaAluno1 = calcular.calcularMedia();
        double mediaAluno2 = calcular2.calcularMedia();

        System.out.println(">>> Média dos Alunos <<<");
        System.out.println("> Aluno 1: " + mediaAluno1);
        System.out.println("> Aluno 2: " + mediaAluno2);
    }
}
