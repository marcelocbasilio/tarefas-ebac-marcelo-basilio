package mod11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MainCollectionTaskOne {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("> Digite nomes separados por vírgula <");
        String nomes = sc.nextLine();
        List<String> listaNomes = new ArrayList<>(List.of(nomes.split(",")));

        Collections.sort(listaNomes);
        System.out.println("> Nomes digitados em ordem alfabética <");
        System.out.println(listaNomes);

        sc.close();
    }
}
