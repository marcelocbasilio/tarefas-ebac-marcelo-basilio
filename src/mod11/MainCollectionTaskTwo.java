package mod11;

import java.util.Scanner;

public class MainCollectionTaskTwo {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("> Digite nome de pessoa separado por traço e para cada nova pessoa separar por vírgula <");
        String nomesSexos = sc.nextLine();

        String[] todosNomesComSexo = nomesSexos.split(",");
        for (String resultado : todosNomesComSexo) {
            String nome = resultado.trim();
            String nomeSemSexo = nome.substring(0, nome.length() - 2);
            if (nome.endsWith("-f") || nome.endsWith("-F")) {
                System.out.println("Nome: " + nomeSemSexo + ", Sexo: Feminino");
            } else if (nome.endsWith("-m") || nome.endsWith("-M")) {
                System.out.println("Nome: " + nomeSemSexo + ", Sexo: Masculino");
            } else {
                System.out.println("Nome: " + nome + ", Sexo: Indefinido");
            }
        }
        sc.close();
    }
}
