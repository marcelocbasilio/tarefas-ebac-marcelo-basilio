package mod24.src.main.java.com.marcelocbasilio.service;

public interface ICustomerService {

    String save();
    String update();
    String delete();
    String findAll();
}
