package mod24.src.main.java.com.marcelocbasilio.service;

public interface IContractService {

    String save();
    String update();
    String delete();
    String findAll();
}
