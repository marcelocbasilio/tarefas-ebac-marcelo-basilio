package mod24.src.main.java.com.marcelocbasilio.service;

import mod24.src.main.java.com.marcelocbasilio.dao.IContractDao;

public class ContractService implements IContractService {

    private IContractDao contractDao;

    public ContractService(IContractDao contractDao) {
        this.contractDao = contractDao;
    }

    @Override
    public String save() {
        contractDao.save();
        return "Success";
//        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }

    @Override
    public String update() {
        contractDao.update();
        return "Contract updated successfully!";
    }

    @Override
    public String delete() {
        contractDao.delete();
        return "Contract Deleted!";
    }

    @Override
    public String findAll() {
        contractDao.findAll();
        return "Seeking Contract!";
    }
}
