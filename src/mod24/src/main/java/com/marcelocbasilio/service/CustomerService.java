package mod24.src.main.java.com.marcelocbasilio.service;

import mod24.src.main.java.com.marcelocbasilio.dao.ICustomerDao;

public class CustomerService implements ICustomerService {

    private ICustomerDao customerDao;

    public CustomerService(ICustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    public String save() {
        customerDao.save();
        return "Success";
//        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }

    @Override
    public String update() {
        customerDao.update();
        return "Customer updated successfully!";
    }

    @Override
    public String delete() {
        customerDao.delete();
        return "Customer Deleted!";
    }

    @Override
    public String findAll() {
        customerDao.findAll();
        return "Seeking Customer!";
    }
}
