package mod24.src.main.java.com.marcelocbasilio.dao;

public interface ICustomerDao {

    void save();
    void update();
    void delete();
    void findAll();
}
