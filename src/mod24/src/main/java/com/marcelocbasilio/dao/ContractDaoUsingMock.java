package mod24.src.main.java.com.marcelocbasilio.dao;

public class ContractDaoUsingMock implements IContractDao {

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    @Override
    public void findAll() {

    }
}
