package mod24.src.main.java.com.marcelocbasilio.dao;

public class CustomerDaoUsingMock implements ICustomerDao {

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }

    @Override
    public void findAll() {

    }
}
