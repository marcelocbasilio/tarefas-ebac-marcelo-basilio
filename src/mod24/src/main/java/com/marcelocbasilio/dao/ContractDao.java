package mod24.src.main.java.com.marcelocbasilio.dao;

public class ContractDao implements IContractDao {

    @Override
    public void save() {
        throw new UnsupportedOperationException("No access to the Database.");
    }

    @Override
    public void update() {
        throw new UnsupportedOperationException("No access to the Database.");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("No access to the Database.");
    }

    @Override
    public void findAll() {
        throw new UnsupportedOperationException("No access to the Database.");
    }
}
