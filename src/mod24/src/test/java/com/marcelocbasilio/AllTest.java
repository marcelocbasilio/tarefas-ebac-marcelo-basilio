package mod24.src.test.java.com.marcelocbasilio;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ContractServiceTest.class, CustomerServiceTest.class})
public class AllTest {
}