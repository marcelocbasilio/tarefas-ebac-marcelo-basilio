package mod24.src.test.java.com.marcelocbasilio;

import mod24.src.main.java.com.marcelocbasilio.dao.CustomerDao;
import mod24.src.main.java.com.marcelocbasilio.dao.CustomerDaoUsingMock;
import mod24.src.main.java.com.marcelocbasilio.dao.ICustomerDao;
import mod24.src.main.java.com.marcelocbasilio.service.CustomerService;
import mod24.src.main.java.com.marcelocbasilio.service.ICustomerService;
import org.junit.Assert;
import org.junit.Test;

public class CustomerServiceTest {

    @Test
    public void saveTest() {
        ICustomerDao customerDao = new CustomerDaoUsingMock();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.save();
        Assert.assertEquals("Success", result);
    }

    @Test
    public void findAllTest() {
        ICustomerDao customerDao = new CustomerDaoUsingMock();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.findAll();
        Assert.assertEquals("Seeking Customer!", result);
    }

    @Test
    public void deleteTest() {
        ICustomerDao customerDao = new CustomerDaoUsingMock();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.delete();
        Assert.assertEquals("Customer Deleted!", result);
    }

    @Test
    public void updateTest() {
        ICustomerDao customerDao = new CustomerDaoUsingMock();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.update();
        Assert.assertEquals("Customer updated successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void saveNullTest() {
        ICustomerDao customerDao = new CustomerDao();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.save();
        Assert.assertEquals("Customer saved successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void updateNullTest() {
        ICustomerDao customerDao = new CustomerDao();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.update();
        Assert.assertEquals("Customer updated successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deleteNullTest() {
        ICustomerDao customerDao = new CustomerDao();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.delete();
        Assert.assertEquals("Customer deleted successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findAllNullTest() {
        ICustomerDao customerDao = new CustomerDao();
        ICustomerService customerService = new CustomerService(customerDao);
        String result = customerService.findAll();
        Assert.assertEquals("Seeking Customer!", result);
    }
}
