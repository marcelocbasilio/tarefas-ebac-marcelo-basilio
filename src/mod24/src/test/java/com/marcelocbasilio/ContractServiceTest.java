package mod24.src.test.java.com.marcelocbasilio;

import mod24.src.main.java.com.marcelocbasilio.dao.ContractDao;
import mod24.src.main.java.com.marcelocbasilio.dao.ContractDaoUsingMock;
import mod24.src.main.java.com.marcelocbasilio.dao.IContractDao;
import mod24.src.main.java.com.marcelocbasilio.service.ContractService;
import mod24.src.main.java.com.marcelocbasilio.service.IContractService;
import org.junit.Assert;
import org.junit.Test;

public class ContractServiceTest {

    @Test
    public void saveTest() {
        IContractDao contractDao = new ContractDaoUsingMock();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.save();
        Assert.assertEquals("Success", result);
    }

    @Test
    public void findAllTest() {
        IContractDao contractDao = new ContractDaoUsingMock();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.findAll();
        Assert.assertEquals("Seeking Contract!", result);
    }

    @Test
    public void deleteTest() {
        IContractDao contractDao = new ContractDaoUsingMock();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.delete();
        Assert.assertEquals("Contract Deleted!", result);
    }

    @Test
    public void updateTest() {
        IContractDao contractDao = new ContractDaoUsingMock();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.update();
        Assert.assertEquals("Contract updated successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void saveNullTest() {
        IContractDao contractDao = new ContractDao();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.save();
        Assert.assertEquals("Contract saved successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void updateNullTest() {
        IContractDao contractDao = new ContractDao();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.update();
        Assert.assertEquals("Contract updated successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void deleteNullTest() {
        IContractDao contractDao = new ContractDao();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.delete();
        Assert.assertEquals("Contract deleted successfully!", result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void findAllNullTest() {
        IContractDao contractDao = new ContractDao();
        IContractService contractService = new ContractService(contractDao);
        String result = contractService.findAll();
        Assert.assertEquals("Seeking Contract!", result);
    }

}
