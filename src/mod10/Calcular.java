package mod10;

/**
 * @author Marcelo Basílio
 * @since 2024-06-29
 */
public class Calcular {

    private double nota1, nota2, nota3, nota4;

    public Calcular() {
    }

    public Calcular(double nota1, double nota2, double nota3, double nota4) {
        this.setNota1(nota1);
        this.setNota2(nota2);
        this.setNota3(nota3);
        this.setNota4(nota4);
    }

    /**
     * Método informa situação do aluno.
     */
    public String informarSituacao() {
        double media = this.calcularMedia();
        if (media >= 5.0d) {
            if (media >= 7.0d) {
                return "APROVADO!";
            }
            return "RECUPERAÇÃO!";
        }
        return "REPROVADO!";
    }

    /**
     * @return a média das notas informadas.
     */
    public double calcularMedia() {
        return this.somar() / 4.0;
    }

    /**
     * @return a soma das quatro notas informada
     */
    private double somar() {
        return (this.getNota1() + this.getNota2() + this.getNota3() + this.getNota4());
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

    public double getNota4() {
        return nota4;
    }

    public void setNota4(double nota4) {
        this.nota4 = nota4;
    }
}
