package mod10;

public class Main {

    public static void main(String[] args) {

        Calcular calcular = new Calcular();
        calcular.setNota1(6.5d);
        calcular.setNota2(8.5d);
        calcular.setNota3(5.5d);
        calcular.setNota4(6.5d);

        Calcular calcular2 = new Calcular(4.1d, 6.3d, 5.5d, 3.5d);
        Calcular calcular3 = new Calcular(10.0d, 10.0d, 10.0d, 10.0d);

        double mediaAluno1 = calcular.calcularMedia();
        double mediaAluno2 = calcular2.calcularMedia();
        double mediaAluno3 = calcular3.calcularMedia();
        String situacaoAluno1 = calcular.informarSituacao();
        String situacaoAluno2 = calcular2.informarSituacao();
        String situacaoAluno3 = calcular3.informarSituacao();

        System.out.println(">>> Alunos <<<");
        System.out.println("> Média 1: " + mediaAluno1 + " > Situação: " + situacaoAluno1);
        System.out.println("> Média 2: " + mediaAluno2 + " > Situacao: " + situacaoAluno2);
        System.out.println("> Média 3: " + mediaAluno3 + " > Situacao: " + situacaoAluno3);
    }
}
