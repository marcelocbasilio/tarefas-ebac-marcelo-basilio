package mod12;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("> Digite nome de pessoa separado por traço e para cada nova pessoa separar por vírgula <");
        String nomesSexos = sc.nextLine();

        String[] todosNomesComSexo = nomesSexos.split(",");

        List<String> arrayMasculino = new ArrayList<>();
        List<String> arrayFeminino = new ArrayList<>();

        for (String resultado : todosNomesComSexo) {
            String nome = resultado.trim();
            String nomeSemSexo = nome.substring(0, nome.length() - 2);
            if (nome.endsWith("-m") || nome.endsWith("-M")) {
                arrayMasculino.add(nomeSemSexo);
            } else if (nome.endsWith("-f") || nome.endsWith("-F")) {
                arrayFeminino.add(nomeSemSexo);
            } else {
                System.out.println("Nome: " + nome + ", Sexo: Indefinido");
            }
        }

        System.out.println("> Sexo Masculino: " + arrayMasculino);
        System.out.println("> Sexo Feminino: " + arrayFeminino);
    }
}
