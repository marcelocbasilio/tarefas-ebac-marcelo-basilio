package mod9;

import java.util.Scanner;

public class BoxingUnboxingConsole {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("> Digite um valor numérico: ");
        long c = sc.nextLong();
        System.out.println("\nBoxing");
        Long d = Long.valueOf(c);

        System.out.println("Primitivo: " + c);
        System.out.println("Wrapper: " + d);

        System.out.println("\n> Digite um valor numérico: ");
        Long e = sc.nextLong();
        System.out.println("\nUnboxing");
        long f = e;
        System.out.println("Wrapper: " + e);
        System.out.println("Primitivo: " + f);

        sc.close();
    }
}
