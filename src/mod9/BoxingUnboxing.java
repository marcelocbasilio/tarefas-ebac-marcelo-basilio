package mod9;

public class BoxingUnboxing {

    public static void main(String[] args) {

        System.out.println("Boxing");
        long x = 9223372036854775807L;
        Long y = Long.valueOf(x);

        System.out.println("Primitivo: " + x);
        System.out.println("Wrapper: " + y);

        System.out.println("\nUnboxing");
        Long a = Long.MAX_VALUE;
        long b = a;

        System.out.println("Wrapper: " + a);
        System.out.println("Primitivo: " + b);
    }

}
