package mod19.entities;

import mod19.annotation.Table;

import java.util.UUID;

@Table(name = "tb_products")
public class Product {

    private UUID id;
    private String name;
    private String description;
    private Double price;
    private String barcode;

    public Product() {
    }

    public Product(UUID id, String name, String description, Double price, String barcode) {
        this.setId(id);
        this.setName(name);
        this.setDescription(description);
        this.setPrice(price);
        this.setBarcode(barcode);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;
        return id.equals(product.id) && barcode.equals(product.barcode);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + barcode.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", description='" + description + '\'' +
               ", price=" + price +
               ", barcode='" + barcode + '\'' +
               '}';
    }
}
