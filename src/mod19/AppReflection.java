package mod19;

import mod19.annotation.Table;
import mod19.entities.Product;

import java.lang.annotation.Annotation;

public class AppReflection {

    public static void main(String[] args) {

        Class<Product> productClass = Product.class;
        Annotation[] annotations = productClass.getAnnotations();

        for (Annotation annotation : annotations) {

            if (annotation instanceof Table tableAnnotation) {
                String product = tableAnnotation.name();
                System.out.println("Product Name: " + product);
            }
        }
    }
}
