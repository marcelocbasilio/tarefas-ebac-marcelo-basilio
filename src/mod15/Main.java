package mod15;

import mod15.domain.Customer;
import mod15.domain.abstracts.AbstractFactory;
import mod15.domain.enums.Acquisition;
import mod15.domain.enums.CarType;
import mod15.factories.CarFactory;
import mod15.factories.CarRental;

public class Main {
    public static void main(String[] args) {
        Customer john = new Customer(CarType.SPORT, Acquisition.BUY);
        AbstractFactory afJohn = getAbstractFactory(john);
        afJohn.createCar(john.getCarType());

        Customer mary = new Customer(CarType.WORK, Acquisition.RENT);
        AbstractFactory afMary = getAbstractFactory(mary);
        afMary.createCar(mary.getCarType());

        Customer zac = new Customer(CarType.STUDENT, Acquisition.BUY);
        AbstractFactory afZac = getAbstractFactory(zac);
        afZac.createCar(zac.getCarType());
    }

    private static AbstractFactory getAbstractFactory(Customer customer) {
        if (customer.getAcquisition() == Acquisition.BUY) {
            return new CarFactory();
        } else {
            return new CarRental();
        }
    }
}
