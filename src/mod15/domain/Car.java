package mod15.domain;

public abstract class Car {

    private final String model;
    private final String color;
    private final int year;

    public Car(String model, String color, int year) {
        this.model = model;
        this.color = color;
        this.year = year;
    }

    public void clean() {
        System.out.printf("> The car %s with color %s is clean.%n", model, color);
    }

    public void readyCar() {
        System.out.printf("> The %s, year %d, passed all tests and is ready for sale.%n", model, year);
    }
}
