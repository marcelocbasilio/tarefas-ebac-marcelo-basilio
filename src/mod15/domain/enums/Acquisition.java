package mod15.domain.enums;

public enum Acquisition {
    BUY, RENT;
}
