package mod15.domain.enums;

public enum CarType {
    SPORT, WORK, STUDENT;
}
