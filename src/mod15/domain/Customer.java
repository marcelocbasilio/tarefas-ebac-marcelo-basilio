package mod15.domain;

import mod15.domain.enums.Acquisition;
import mod15.domain.enums.CarType;

public class Customer {

    private CarType carType;
    private Acquisition acquisition;

    public Customer(CarType carType, Acquisition acquisition) {
        this.setCarType(carType);
        this.setAcquisition(acquisition);
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public Acquisition getAcquisition() {
        return acquisition;
    }

    public void setAcquisition(Acquisition acquisition) {
        this.acquisition = acquisition;
    }
}
