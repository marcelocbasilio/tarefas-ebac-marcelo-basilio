package mod15.domain.abstracts;

import mod15.domain.Car;
import mod15.domain.enums.CarType;

public abstract class AbstractFactory {

    public abstract Car receiveCar(CarType type);

    public Car createCar(CarType carType) {
        Car car = receiveCar(carType);
        prepareCar(car);
        return car;
    }

    private Car prepareCar(Car car) {
        car.clean();
        car.readyCar();
        return car;
    }
}
