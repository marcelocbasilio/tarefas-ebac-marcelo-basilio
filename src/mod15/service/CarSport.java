package mod15.service;

import mod15.domain.Car;

public class CarSport extends Car {
    public CarSport(String model, String color, int year) {
        super(model, color, year);
    }
}
