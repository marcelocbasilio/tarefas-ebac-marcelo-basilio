package mod15.service;

import mod15.domain.Car;

public class CarStudent extends Car {
    public CarStudent(String model, String color, int year) {
        super(model, color, year);
    }
}
