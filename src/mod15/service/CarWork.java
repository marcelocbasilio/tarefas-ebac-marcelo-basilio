package mod15.service;

import mod15.domain.Car;

public class CarWork extends Car {
    public CarWork(String model, String color, int year) {
        super(model, color, year);
    }
}
