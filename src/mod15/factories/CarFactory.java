package mod15.factories;

import mod15.domain.Car;
import mod15.domain.abstracts.AbstractFactory;
import mod15.domain.enums.CarType;
import mod15.service.CarSport;
import mod15.service.CarWork;
import mod15.service.CarStudent;

public class CarFactory extends AbstractFactory {

    @Override
    public Car receiveCar(CarType type) {
        if (type == CarType.SPORT) {
            return new CarSport("Skyline RS", "Yellow", 2011);
        } else if (type == CarType.WORK) {
            return new CarWork("Hilux Cab Single", "White", 2020);
        } else if (type == CarType.STUDENT) {
            return new CarStudent("Corolla", "blue artic", 2022);
        } else {
            System.out.println("Car no available!");
            return null;
        }
    }
}
