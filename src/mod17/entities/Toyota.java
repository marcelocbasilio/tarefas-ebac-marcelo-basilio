package mod17.entities;

public class Toyota extends Car {
    public Toyota(String model, String color, int year) {
        super(model, color, year);
    }
}
