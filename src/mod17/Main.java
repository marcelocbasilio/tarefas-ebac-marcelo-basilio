package mod17;

import mod17.entities.Audi;
import mod17.entities.Toyota;
import mod17.service.CarFactory;

@SuppressWarnings("rawtypes")
public class Main {

    public static void main(String[] args) {

        CarFactory carFactory = new CarFactory();

        Toyota hilux = new Toyota("Hilux SRV", "Black", 2011);
        Toyota corolla = new Toyota("Corolla XEI", "White", 2021);
        Audi rs6 = new Audi("Audi RS6 4.0 V8 TFSI", "Black", 2024);
        Audi q8 = new Audi("Audi Q8 e-tron", "Pearl", 2025);

        carFactory.storeCar(hilux);
        carFactory.storeCar(corolla);
        carFactory.storeCar(rs6);
        carFactory.storeCar(q8);

        carFactory.reportCarsInGarage();


    }
}
