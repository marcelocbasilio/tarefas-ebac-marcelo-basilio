package mod7;

/**
 * @author Marcelo Basílio
 * @since 2024-06-28
 */
public class Main {
    public static void main(String[] args) {
        Endereco endereco = new Endereco();
        endereco.setLogradouro("Rua da Silva");
        endereco.setNumero("3232");
        endereco.setComplemento("Sabiaguaba");
        endereco.setBairro("Taquara");
        endereco.setCidade("Varzea");
        endereco.setUf("CE");

        Pessoa pessoa = new Pessoa("Márcio André", 34, "+55 85 9 9876-5432", "marcio@teste.com", endereco);

        pessoa.imprimirDados();
    }
}
