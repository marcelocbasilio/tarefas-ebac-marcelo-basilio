package mod7;

/**
 * @author Marcelo Basílio
 * @since 2024-06-28
 */
public class Pessoa {
    private String nome;
    private int idade;
    private String telefone;
    private String email;
    private Endereco endereco;

    public Pessoa() {
    }

    public Pessoa(String nome, int idade, String telefone, String email, Endereco endereco) {
        this.nome = nome;
        this.idade = idade;
        this.telefone = telefone;
        this.email = email;
        this.endereco = endereco;
    }

    public void imprimirDados() {
        System.out.println(">>> DADOS DA PESSOA <<<");
        System.out.println("Nome: " + nome);
        System.out.println("Idade: " + idade + " anos");
        System.out.println("Telefone: " + telefone);
        System.out.println("Email: " + email);
        System.out.println("\n>>> ENDEREÇO DA PESSOA <<<");
        System.out.println("Logradouro: " + endereco.getLogradouro());
        System.out.println("Número: " + endereco.getNumero());
        System.out.println("Complemento: " + endereco.getComplemento());
        System.out.println("Bairro: " + endereco.getBairro());
        System.out.println("Cidade: " + endereco.getCidade());
        System.out.println("UF: " + endereco.getUf());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
