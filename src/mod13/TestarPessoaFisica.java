package mod13;

import mod13.entities.Endereco;
import mod13.entities.PessoaFisica;
import mod13.entities.enums.EstadoCivil;
import mod13.entities.enums.Sexo;

import java.time.LocalDate;

public class TestarPessoaFisica {

    public static void main(String[] args) {

        PessoaFisica pf = new PessoaFisica();
        Endereco endPf = new Endereco();

        endPf.setCep("01234-567");
        endPf.setLogradouro("Rua da Paz");
        endPf.setNumero("12345");
        endPf.setComplemento("Condominio São Paulo");
        endPf.setBairro("Bairro Assunção");
        endPf.setCidade("Araraquara");
        endPf.setUf("DF");

        pf.setNome("Marcelo");
        pf.setCpf("123.456.789-01");
        pf.setRg("12345678901234");
        pf.setDataNascimento(LocalDate.now().minusYears(40));
        pf.setSexo(Sexo.MASCULINO);
        pf.setNacionalidade("Brasileiro");
        pf.setEstadoCivil(EstadoCivil.CASADO);
        pf.setCelular("+55 01 12345-6789");
        pf.setEmail("marcelo@gmail.com");
        pf.setEndereco(endPf);

        pf.imprimirDados();

    }
}
