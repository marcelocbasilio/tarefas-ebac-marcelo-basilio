package mod13.entities;

import java.time.LocalDate;
import java.util.Objects;

public class PessoaJuridica extends Pessoa {

    private String cnpj;
    private String inscricaoEstadual;
    private String inscricaoMunicipal;
    private LocalDate dataAbertura;
    private String celular;
    private String email;
    private String url;

    public PessoaJuridica() {}

    public PessoaJuridica(String cnpj, String inscricaoEstadual, String inscricaoMunicipal, LocalDate dataAbertura, String celular, String email, String url) {
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.inscricaoMunicipal = inscricaoMunicipal;
        this.dataAbertura = dataAbertura;
        this.celular = celular;
        this.email = email;
        this.url = url;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getInscricaoMunicipal() {
        return inscricaoMunicipal;
    }

    public void setInscricaoMunicipal(String inscricaoMunicipal) {
        this.inscricaoMunicipal = inscricaoMunicipal;
    }

    public LocalDate getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(LocalDate dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PessoaJuridica that = (PessoaJuridica) o;
        return Objects.equals(cnpj, that.cnpj);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cnpj);
    }

    @Override
    public String toString() {
        return "PessoaJuridica{" +
               "cnpj='" + cnpj + '\'' +
               ", inscricaoEstadual='" + inscricaoEstadual + '\'' +
               ", inscricaoMunicipal='" + inscricaoMunicipal + '\'' +
               ", dataAbertura=" + dataAbertura +
               ", celular='" + celular + '\'' +
               ", email='" + email + '\'' +
               ", url='" + url + '\'' +
               '}';
    }


    @Override
    public void imprimirDados() {
        System.out.println(">>> Dados da Pessoa Jurídica <<<");
        System.out.println("> Nome: " + super.getNome());
        System.out.println("> CNPJ: " + this.getCnpj());
        System.out.println("> Insc. Estadual: " + this.getInscricaoEstadual());
        System.out.println("> Insc. Municipal: " + this.getInscricaoMunicipal());
        System.out.println("> Data Abertura: " + this.getDataAbertura());
        System.out.println("> Celular: " + this.getCelular());
        System.out.println("> E-mail: " + this.getEmail());
        System.out.println("> Site: " + this.getUrl());
        this.getEndereco().imprimirDados();
    }
}
