package mod13.entities.enums;

public enum EstadoCivil {
    SOLTEIRO, CASADO, DIVORCIADO, VIUVO;
}
