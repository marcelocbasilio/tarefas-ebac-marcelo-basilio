package mod13.entities;

import mod13.entities.enums.EstadoCivil;
import mod13.entities.enums.Sexo;

import java.time.LocalDate;
import java.util.Objects;

public class PessoaFisica extends Pessoa {

    private String cpf;
    private String rg;
    private LocalDate dataNascimento;
    private Sexo sexo;
    private String nacionalidade;
    private EstadoCivil estadoCivil;
    private String celular;
    private String email;

    public PessoaFisica() {}

    public PessoaFisica(String cpf, String rg, LocalDate dataNascimento, Sexo sexo, String nacionalidade, EstadoCivil estadoCivil, String celular, String email) {
        this.cpf = cpf;
        this.rg = rg;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.nacionalidade = nacionalidade;
        this.estadoCivil = estadoCivil;
        this.celular = celular;
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PessoaFisica that = (PessoaFisica) o;
        return Objects.equals(cpf, that.cpf);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cpf);
    }

    @Override
    public String toString() {
        return "PessoaFisica{" +
               "cpf='" + cpf + '\'' +
               ", rg='" + rg + '\'' +
               ", dataNascimento=" + dataNascimento +
               ", sexo=" + sexo +
               ", nacionalidade='" + nacionalidade + '\'' +
               ", estadoCivil=" + estadoCivil +
               ", celular='" + celular + '\'' +
               ", email='" + email + '\'' +
               '}';
    }

    @Override
    public void imprimirDados() {
        System.out.println(">>> Dados da Pessoa Física <<<");
        System.out.println("> Nome: " + super.getNome());
        System.out.println("> CPF: " + this.getCpf());
        System.out.println("> RG: " + this.getRg());
        System.out.println("> Data Nascimento: " + this.getDataNascimento());
        System.out.println("> Sexo: " + this.getSexo());
        System.out.println("> Nacionalidade: " + this.getNacionalidade());
        System.out.println("> Estado Civil: " + this.getEstadoCivil());
        System.out.println("> Celular: " + this.getCelular());
        System.out.println("> E-mail: " + this.getEmail());
        this.getEndereco().imprimirDados();
    }
}
