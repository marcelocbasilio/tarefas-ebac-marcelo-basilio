package mod13.entities;

import java.util.Objects;

public class Endereco {

    private Long id;
    private String cep;
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String uf;

    public Endereco() {}

    public Endereco(Long id, String cep, String logradouro, String numero, String complemento, String bairro, String cidade, String uf) {
        this.id = id;
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Endereco endereco = (Endereco) o;
        return Objects.equals(id, endereco.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Endereco{" +
               "id=" + id +
               ", cep='" + cep + '\'' +
               ", logradouro='" + logradouro + '\'' +
               ", numero='" + numero + '\'' +
               ", complemento='" + complemento + '\'' +
               ", bairro='" + bairro + '\'' +
               ", cidade='" + cidade + '\'' +
               ", uf='" + uf + '\'' +
               '}';
    }

    public void imprimirDados() {
        System.out.println(">>> Endereço <<<");
        System.out.println("> CEP: " + this.getCep());
        System.out.println("> Logradouro: " + this.getLogradouro());
        System.out.println("> Número: " + this.getNumero());
        System.out.println("> Complemento: " + this.getComplemento());
        System.out.println("> Bairro: " + this.getBairro());
        System.out.println("> Cidade: " + this.getCidade());
        System.out.println("> UF: " + this.getUf());
    }
}
