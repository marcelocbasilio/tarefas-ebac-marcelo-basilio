package mod13;

import mod13.entities.Endereco;
import mod13.entities.PessoaJuridica;

import java.time.LocalDate;

public class TestarPessoaJuridica {

    public static void main(String[] args) {

        PessoaJuridica pj = new PessoaJuridica();
        Endereco endPj = new Endereco();

        endPj.setCep("98765-432");
        endPj.setLogradouro("Rua da Empresa");
        endPj.setNumero("678901");
        endPj.setComplemento("Ilha Dungeon");
        endPj.setBairro("Bairro Couremmant");
        endPj.setCidade("Piraporã");
        endPj.setUf("GO");

        pj.setNome("MAR");
        pj.setCnpj("01.234.567/8901-23");
        pj.setInscricaoEstadual("01.23456.78-9");
        pj.setInscricaoMunicipal("0123456789-0");
        pj.setDataAbertura(LocalDate.now().minusYears(10));
        pj.setCelular("+55 01 98765-4321");
        pj.setEmail("contato@mar.com.br");
        pj.setUrl("https://mar.com.br");
        pj.setEndereco(endPj);

        pj.imprimirDados();
    }
}
