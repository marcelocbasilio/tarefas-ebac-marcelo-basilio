package mod22;

import javax.swing.*;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        String title = "[mod22] Stream";
        String message = "Digite nomes e sexo (-m ou -f) separados por vírgula!";
        String listOfName = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);

        List<String> people = List.of(listOfName.split(","));

        System.out.println("> Lista de Mulheres <");
        people.stream().filter(s -> s.endsWith("-f") || s.endsWith("-F")).forEach(System.out::println);

        System.exit(0);
    }
}
